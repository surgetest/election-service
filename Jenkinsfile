@NonCPS
def validateTriggerCause() {
    if (isPullRequestCause()) return true
    if (isTimerTriggerCause()) return true
    if (isBranchIndexingCause()) return true
    if (isUserTriggerCause()) {
        def userIdCause = currentBuild.rawBuild.getCause(hudson.model.Cause.UserIdCause)
        if (isValidUser(userIdCause.getUserName())) return true
        else error "[FAILED] ${userIdCause.getUserName()} is not allowed to trigger this pipeline"
    }
    error '[FAILED] Invalid pipeline trigger cause'
}

@NonCPS
def isPullRequestCause() {
    def pullRequestCauses = ['pullrequest:created', 'pullrequest:updated', 'pullrequest:fulfilled']
    def genericCause = currentBuild.rawBuild.getCause(org.jenkinsci.plugins.gwt.GenericCause)
    return genericCause != null && env.x_event_key in pullRequestCauses
}

@NonCPS
def isPullRequestMergedCause() {
    return isPullRequestCause() && env.x_event_key == 'pullrequest:fulfilled'
}

@NonCPS
def isUserTriggerCause() {
    def userIdCause = currentBuild.rawBuild.getCause(hudson.model.Cause.UserIdCause)
    return userIdCause != null
}

@NonCPS
def isTimerTriggerCause() {
    def timerTriggerCause = currentBuild.rawBuild.getCause(hudson.triggers.TimerTrigger.TimerTriggerCause)
    return timerTriggerCause != null
}

@NonCPS
def isBranchIndexingCause() {
    def branchIndexingCause = currentBuild.rawBuild.getCause(jenkins.branch.BranchIndexingCause)
    return branchIndexingCause != null
}

def isValidUser(user) {
    def allowedUsers = ['admin', 'alex', 'matt', 'taiwo']
    return user.toLowerCase() in allowedUsers
}

def prettyPrint(object) {
    println groovy.json.JsonOutput.prettyPrint(groovy.json.JsonOutput.toJson(object))
}

def customRetry(int retries, Closure body) {
    for(int i = 0; i < retries; i++) {
        try {
            body.call()
            break
        } catch (org.jenkinsci.plugins.workflow.steps.FlowInterruptedException fie) {
            echo "${fie}"
            continue
        } catch (e) {
            throw e
        }
    }
}

def runtimeParams = new LinkedHashMap();

def commitLabel = "Commit on ${env.BRANCH_NAME} (optional)"
def uploadArtifactsLabel = 'Upload Build Artefact'
def projectVersionLabel = 'Project Version (optional)'

pipeline {
    agent any
    options {
        buildDiscarder(logRotator(numToKeepStr:'30'))
    }
    parameters {
        string(name: commitLabel, defaultValue: '', description: 'Commit (optional) e.g. 44c2e9c')
        booleanParam(name: uploadArtifactsLabel, defaultValue: false, description: 'Select to upload artefacts to Artifactory')
        string(name: projectVersionLabel, defaultValue: '', description: 'Project Version (optional) e.g. 1.0.0')
    }
    triggers {
        GenericTrigger(
            genericVariables: [
                [key: 'targetBranch', value: '$.pullrequest.destination.branch.name'],
                [key: 'sourceBranch', value: '$.pullrequest.source.branch.name'],
                [key: 'author', value: '$.pullrequest.author.username'],
            ],
            genericHeaderVariables: [
                [key: 'X-Event-Key', regexpFilter: '']
            ],
            causeString: 'Started by Bitbucket event ($x_event_key)',
            token: 'R7InLsjTtgyi9kDx',
            printContributedVariables: false,
            printPostContent: false,
            regexpFilterText: '$targetBranch',
            regexpFilterExpression: env.BRANCH_NAME
        )
        cron('0 11,23 * * 1-5') // Triggers the build weekdays 11AM and 11PM
    }
    stages {
        stage('Preparation') {
            steps {
                echo 'Aligning the planets and moons!'
                script {
                    lastStage = env.STAGE_NAME
                    // validate trigger cause
                    validateTriggerCause()
                    runtimeParams.repositoryName = 'election-service'
                    runtimeParams.repositoryOwner = 'surgetest'
                    runtimeParams.targetBranch = env.BRANCH_NAME
                    runtimeParams.commit = params[commitLabel]?.trim()
                    runtimeParams.projectVersion = params[projectVersionLabel]?.trim()
                    if (!runtimeParams.projectVersion) {
                        runtimeParams.projectVersion = sh script: "./gradlew getVersion --no-daemon -q", returnStdout: true
                        runtimeParams.projectVersion = runtimeParams.projectVersion?.trim()
                    }
                    runtimeParams.artifactVersion = runtimeParams.projectVersion + '-BUILD-' + env.BUILD_NUMBER + (runtimeParams.targetBranch.startsWith('rc')  ? '-RC' : '-SNAPSHOT')
                    runtimeParams.uploadArtifacts = params[uploadArtifactsLabel] || isTimerTriggerCause() || (isPullRequestCause() && isPullRequestMergedCause())
                    runtimeParams.gitUrl = "git@bitbucket.org:${runtimeParams.repositoryOwner}/${runtimeParams.repositoryName}.git"
                    runtimeParams.artifactoryServerUrl = 'http://35.204.248.170/artifactory'
                    if (isPullRequestCause()) {
                        runtimeParams.sourceBranch = env.sourceBranch
                        runtimeParams.author = env.author
                    }
                    prettyPrint(runtimeParams)
                }
            }
        }
        stage('SCM') {
            steps {
                script {
                    lastStage = env.STAGE_NAME
                    if (isPullRequestCause() && !isPullRequestMergedCause()) {
                        git branch: runtimeParams.sourceBranch, url: runtimeParams.gitUrl, changelog: true, credentialsId: 'jenkins-generated-ssh-key'
                        sh """
                            git checkout ${runtimeParams.targetBranch}
                            git config user.email ${runtimeParams.author}@jenkins.build
                            git config user.name ${runtimeParams.author}
                            git merge origin/${runtimeParams.sourceBranch} --no-commit --stat --strategy=recursive
                        """
                    } else if (runtimeParams.commit) {
                        sh """
                            git checkout ${runtimeParams.commit}
                        """
                    }
                }
            }
        }
        stage('Build') {
            steps {
                script {
                    lastStage = env.STAGE_NAME
                }
                // Run gradle build task
                sh "./gradlew clean jar war -PartifactVersion=${runtimeParams.artifactVersion} --no-daemon -q"
                // archive artifacts
                archiveArtifacts artifacts: 'build/**/**.jar', fingerprint: true
                archiveArtifacts artifacts: 'build/**/**.war', fingerprint: true
            }
        }
        stage('Unit Test') {
            steps {
                script {
                    lastStage = env.STAGE_NAME
                }
                // Run the gradle test task
                sh './gradlew test jacocoTestReport --continue --no-daemon -q && touch build/*/*/TEST-*.xml'
                junit 'build/**/TEST-*.xml'
                jacoco buildOverBuild: true, changeBuildStatus: true, maximumBranchCoverage: '80', maximumClassCoverage: '80', maximumComplexityCoverage: '80', maximumInstructionCoverage: '80', maximumLineCoverage: '80', maximumMethodCoverage: '80', minimumBranchCoverage: '1', minimumClassCoverage: '1', minimumComplexityCoverage: '1', minimumInstructionCoverage: '1', minimumLineCoverage: '1', minimumMethodCoverage: '1', skipCopyOfSrcFiles: true
            }
        }
        stage('Quality Gates') {
            failFast true
            parallel {
                stage('SonarQube Analysis') {
                    steps {
                        script {
                            lastStage = env.STAGE_NAME
                            def scannerHome = tool 'SonarQube Scanner';
                            if (isPullRequestCause() && !isPullRequestMergedCause()) {
                                // requires SonarQube Scanner 2.8+  
                                withSonarQubeEnv('SonarQube Server') {
                                    sh "${scannerHome}/bin/sonar-scanner -X -Dsonar.organization=surgetest -Dsonar.projectKey=surgetest_election-service -Dsonar.projectVersion=${runtimeParams.projectVersion} -Dsonar.projectBaseDir=. -Dsonar.sources=src/main/java,src/main/resources -Dsonar.tests=src/test/java -Dsonar.java.libraries=build/libs -Dsonar.java.binaries=build/classes/main -Dsonar.java.test.binaries=build/classes/test -Dsonar.junit.reportPaths=build/test-results/test -Dsonar.java.coveragePlugin=jacoco -Dsonar.jacoco.reportPath=build/jacoco/test.exec -Dsonar.dynamicAnalysis=reuseReports -Dsonar.scm.provider=git -Dsonar.branch.name=${runtimeParams.sourceBranch} -Dsonar.branch.target=${runtimeParams.targetBranch} -Dsonar.links.ci=${env.BUILD_URL}"
                                }
                            } else {
                                // requires SonarQube Scanner 2.8+
                                withSonarQubeEnv('SonarQube Server') {
                                    sh "${scannerHome}/bin/sonar-scanner -X -Dsonar.organization=surgetest -Dsonar.projectKey=surgetest_election-service -Dsonar.projectVersion=${runtimeParams.projectVersion} -Dsonar.projectBaseDir=. -Dsonar.sources=src/main/java,src/main/resources -Dsonar.tests=src/test/java -Dsonar.java.libraries=build/libs -Dsonar.java.binaries=build/classes/main -Dsonar.java.test.binaries=build/classes/test -Dsonar.junit.reportPaths=build/test-results/test -Dsonar.java.coveragePlugin=jacoco -Dsonar.jacoco.reportPath=build/jacoco/test.exec -Dsonar.dynamicAnalysis=reuseReports -Dsonar.scm.provider=git -Dsonar.branch.name=${runtimeParams.targetBranch} -Dsonar.links.ci=${env.BUILD_URL}"
                                }
                            }
                            // wait for quality gate result
                            customRetry (3) {
                                timeout(time: 3, unit: 'MINUTES') { // just in case something goes wrong, pipeline will be killed after a timeout
                                    // works with taskId previously collected by withSonarQubeEnv
                                    def qg = waitForQualityGate()
                                    if (qg.status != 'OK' && isPullRequestCause() && !isPullRequestMergedCause()) {
                                        println "[FAILED] SonarQube quality gate"
                                        error "Pipeline aborted due to quality gate failure: ${qg.status}"
                                    }
                                }
                            }
                        }
                    }
                }
                stage('Smoke Test') {
                    steps {
                        script {
                            lastStage = env.STAGE_NAME
                            println "Setting up smoke tests environment ..."
                            Thread.sleep(30000)
                            println "Running smoke tests ..."
                            Thread.sleep(30000)
                            if (env.BUILD_NUMBER.toInteger() % 2 != 0 && env.BUILD_NUMBER.toInteger() % 3 != 0) {
                                println "[FAILED] Smoke tests"
                                error "Pipeline aborted due to quality gate failure"
                            }
                        }
                    }
                }
            }
        }
        stage('Artifactory') {
            // stage won't be skipped as long as uploadArtifacts == true
            when { expression { runtimeParams.uploadArtifacts } }
            steps {
                script {
                    lastStage = env.STAGE_NAME
                    def server = Artifactory.newServer url: runtimeParams.artifactoryServerUrl, credentialsId: 'artiFactoryCredentials'
                    def buildInfo = Artifactory.newBuildInfo()
                    buildInfo.retention maxBuilds: 30, deleteBuildArtifacts: true, async: true
                    def artifactFilePath = "build/libs/*-${runtimeParams.artifactVersion}.war"
                    def target = "${runtimeParams.repositoryName}/archive/${runtimeParams.projectVersion}/"
                    println "Uploading ${artifactFilePath} ... \n to ${target}"
                    def uploadSpec = """
                        {
                            "files": [{
                                "pattern": "${artifactFilePath}",
                                "target": "${target}"
                            }]
                        }
                    """
                    server.upload spec: uploadSpec, buildInfo: buildInfo
                    server.publishBuildInfo buildInfo
                    if (isPullRequestCause() && isPullRequestMergedCause()) {
                        def promotionConfig = [
                            // Mandatory parameters
                            'buildName'          : buildInfo.name,
                            'buildNumber'        : buildInfo.number,
                            'targetRepo'         : "${runtimeParams.repositoryName}/staging/${runtimeParams.projectVersion}",
                        
                            // Optional parameters
                            'comment'            : 'Promoted build from archive to staging as a release candidate',
                            'sourceRepo'         : "${runtimeParams.repositoryName}/archive/${runtimeParams.projectVersion}",
                            'status'             : 'Released',
                            'includeDependencies': true,
                            'copy'               : true
                        ]
                        Artifactory.addInteractivePromotion server: server, promotionConfig: promotionConfig, displayName: "Do you want to promote ${runtimeParams.artifactVersion} to staging?"
                    }
                }
            }
        }
    }
    post {
        failure {
            bitbucketStatusNotify(buildState: 'FAILED', buildKey: '${BUILD_ID}', buildName: '${JOB_NAME}')
        }
        unstable {
            bitbucketStatusNotify(buildState: 'FAILED', buildKey: '${BUILD_ID}', buildName: '${JOB_NAME}')
        }
        success {
            bitbucketStatusNotify(buildState: 'SUCCESSFUL', buildKey: '${BUILD_ID}', buildName: '${JOB_NAME}')
        }
    }
}