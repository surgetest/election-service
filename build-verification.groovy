@NonCPS
def validateTriggerCause() {
    if (isPullRequestCause()) return true
    if (isTimerTriggerCause()) return true
    if (isUserTriggerCause()) {
        def userIdCause = currentBuild.rawBuild.getCause(hudson.model.Cause.UserIdCause)
        if (isValidUser(userIdCause.getUserName())) return true
        else error "[FAILED] ${userIdCause.getUserName()} is not allowed to trigger this pipeline"
    }
    error '[FAILED] Invalid pipeline trigger cause'
}

@NonCPS
def isPullRequestCause() {
    def pullRequestCauses = ['pullrequest:created', 'pullrequest:updated', 'pullrequest:fulfilled']
    def genericCause = currentBuild.rawBuild.getCause(org.jenkinsci.plugins.gwt.GenericCause)
    return genericCause != null && env.x_event_key in pullRequestCauses
}

@NonCPS
def isPullRequestMergedCause() {
    return isPullRequestCause() && env.x_event_key == 'pullrequest:fulfilled'
}

@NonCPS
def isUserTriggerCause() {
    def userIdCause = currentBuild.rawBuild.getCause(hudson.model.Cause.UserIdCause)
    return userIdCause != null
}

@NonCPS
def isValidUser(user) {
    def allowedUsers = ['admin', 'alex', 'matt', 'taiwo']
    return user.toLowerCase() in allowedUsers
}

@NonCPS
def isTimerTriggerCause() {
    def timerTriggerCause = currentBuild.rawBuild.getCause(hudson.triggers.TimerTrigger.TimerTriggerCause)
    return timerTriggerCause != null
}

def prettyPrint(object) {
    println groovy.json.JsonOutput.prettyPrint(groovy.json.JsonOutput.toJson(object))
}

def customRetry(int retries, Closure body) {
    for(int i = 0; i < retries; i++) {
        try {
            body.call()
            break
        } catch (org.jenkinsci.plugins.workflow.steps.FlowInterruptedException fie) {
            echo "${fie}"
            continue
        } catch (e) {
            throw e
        }
    }
}

def runtimeParams = new LinkedHashMap();

def repositoryNameLabel = 'Repository Name (required)'
def repositoryOwnerLabel = 'Repository Owner (required)'
def targetBranchLabel = 'Target Branch or Commit (required)'
def uploadArtifactsLabel = 'Upload Build Artefact'
def majorVersionLabel = 'Artefact Version'

pipeline {
    agent none
    options {
        buildDiscarder(logRotator(numToKeepStr:'30'))
    }
    parameters {
        string(name: repositoryNameLabel, defaultValue: 'election-service', description: 'Repository Name')
        string(name: repositoryOwnerLabel, defaultValue: 'surgetest', description: 'Repository Owner')
        string(name: targetBranchLabel, defaultValue: 'develop', description: 'Branch/Commit e.g. develop, 44c2e9c')
        booleanParam(name: uploadArtifactsLabel, defaultValue: false, description: 'Select to upload artefacts to Artifactory')
        string(name: majorVersionLabel, defaultValue: '1.0.0', description: 'Artefact Version Label e.g. 1.0.0')
    }
    triggers {
        GenericTrigger(
            genericVariables: [
                [key: 'targetBranch', value: '$.pullrequest.destination.branch.name'],
                [key: 'sourceBranch', value: '$.pullrequest.source.branch.name'],
                [key: 'author', value: '$.pullrequest.author.username'],
                [key: 'repositoryOwner', value: '$.repository.owner.username'],
                [key: 'repositoryName', value: '$.repository.name'],
            ],
            genericHeaderVariables: [
                [key: 'X-Event-Key', regexpFilter: '']
            ],
            causeString: 'Started by Bitbucket event ($x_event_key)',
            token: 'R7InLsjTtgyi9kDx',
            printContributedVariables: false,
            printPostContent: false
        )
        cron('0 11,23 * * 1-5') // Triggers the build weekdays 11AM and 11PM
    }
    stages {
        stage('Preparation') {
            agent any
            steps {
                echo 'Aligning the planets and moons!'
                script {
                    lastStage = env.STAGE_NAME
                    // validate trigger cause
                    validateTriggerCause()
                    runtimeParams.repositoryName = params[repositoryNameLabel]?.trim()
                    runtimeParams.repositoryOwner = params[repositoryOwnerLabel]?.trim()
                    runtimeParams.targetBranch = params[targetBranchLabel]?.trim()
                    runtimeParams.majorVersion = params[majorVersionLabel]?.trim()
                    runtimeParams.artifactVersion = runtimeParams.majorVersion + '-BUILD-' + env.BUILD_NUMBER + (runtimeParams.targetBranch.startsWith('rc')  ? '-RC' : '-SNAPSHOT')
                    runtimeParams.uploadArtifacts = params[uploadArtifactsLabel] || isTimerTriggerCause() || (isPullRequestCause() && isPullRequestMergedCause())
                    runtimeParams.gitUrl = "git@bitbucket.org:${runtimeParams.repositoryOwner}/${runtimeParams.repositoryName}.git"
                    runtimeParams.artifactoryServerUrl = 'http://35.204.248.170/artifactory'
                    if (isPullRequestCause()) {
                        bitbucketStatusNotify(buildState: 'INPROGRESS', buildKey: '${BUILD_ID}', buildName: '${JOB_NAME}')
                        runtimeParams.repositoryName = env.repositoryName
                        runtimeParams.repositoryOwner = env.repositoryOwner
                        runtimeParams.targetBranch = env.targetBranch
                        runtimeParams.sourceBranch = env.sourceBranch
                        runtimeParams.author = env.author
                    }
                    prettyPrint(runtimeParams)
                }
            }
        }
        // TODO: lock the workspace for Git, Build & Test to prevent concurrent runs
        stage('SCM') {
            agent any
            steps {
                script {
                    lastStage = env.STAGE_NAME
                    // if (isPullRequestCause() && !isPullRequestMergedCause()) {
                    //     git branch: runtimeParams.sourceBranch, url: runtimeParams.gitUrl, changelog: true    
                    // }
                    // // Get target branch from Bitbucket
                    // git branch: runtimeParams.targetBranch, url: runtimeParams.gitUrl, changelog: true
                    // if (isPullRequestCause() && !isPullRequestMergedCause()) {
                    //     sh """
                    //         git config user.email ${runtimeParams.author}@jenkins.build
                    //         git config user.name ${runtimeParams.author}
                    //         git merge origin/${runtimeParams.sourceBranch} --no-commit --stat --strategy=recursive
                    //     """
                    // }
                    runtimeParams.gitUrl = "https://jenkins@bitbucket.org/${runtimeParams.repositoryOwner}/${runtimeParams.repositoryName}.git"
                    sh """
                        rm -rf ${runtimeParams.repositoryName}
                        git clone ${runtimeParams.gitUrl}
                        cd ${runtimeParams.repositoryName}
                        git checkout ${runtimeParams.targetBranch}
                    """
                    if (isPullRequestCause() && !isPullRequestMergedCause()) {
                        // Get short-lived branch from Bitbucket
                        sh """
                            cd ${runtimeParams.repositoryName}
                            git checkout ${runtimeParams.targetBranch}
                            git config user.email ${runtimeParams.author}@jenkins.build
                            git config user.name ${runtimeParams.author}
                            git merge origin/${runtimeParams.sourceBranch} --no-commit --stat --strategy=recursive
                        """
                    }
                }
            }
        }
        stage('Build') {
            agent any
            steps {
                script {
                    lastStage = env.STAGE_NAME
                }
                // Run gradle build task
                sh "cd ${runtimeParams.repositoryName} && ./gradlew clean jar war -PartifactVersion=${runtimeParams.artifactVersion} --no-daemon"
                // archive artifacts
                archiveArtifacts artifacts: "${runtimeParams.repositoryName}/build/**/**.jar", fingerprint: true
                archiveArtifacts artifacts: "${runtimeParams.repositoryName}/build/**/**.war", fingerprint: true
            }
        }
        stage('Unit Test') {
            agent any
            steps {
                script {
                    lastStage = env.STAGE_NAME
                }
                // Run the gradle test task
                sh "cd ${runtimeParams.repositoryName} && ./gradlew test jacocoTestReport --continue --no-daemon && touch build/*/*/TEST-*.xml"
                junit "${runtimeParams.repositoryName}/build/**/TEST-*.xml"
                jacoco buildOverBuild: true, changeBuildStatus: true, maximumBranchCoverage: '80', maximumClassCoverage: '80', maximumComplexityCoverage: '80', maximumInstructionCoverage: '80', maximumLineCoverage: '80', maximumMethodCoverage: '80', minimumBranchCoverage: '1', minimumClassCoverage: '1', minimumComplexityCoverage: '1', minimumInstructionCoverage: '1', minimumLineCoverage: '1', minimumMethodCoverage: '1', skipCopyOfSrcFiles: true
            }
        }
        stage('Quality Gates') {
            failFast true
            parallel {
                stage('SonarQube Analysis') {
                    agent any
                    steps {
                        script {
                            lastStage = env.STAGE_NAME
                            def scannerHome = tool 'SonarQube Scanner';
                            if (isPullRequestCause() && !isPullRequestMergedCause()) {
                                // requires SonarQube Scanner 2.8+  
                                withSonarQubeEnv('SonarQube Server') {
                                    sh "${scannerHome}/bin/sonar-scanner -X -Dsonar.organization=surgetest -Dsonar.projectKey=surgetest_election-service -Dsonar.projectBaseDir=./${runtimeParams.repositoryName} -Dsonar.sources=src/main/java,src/main/resources -Dsonar.tests=src/test/java -Dsonar.java.libraries=build/libs -Dsonar.java.binaries=build/classes/main -Dsonar.java.test.binaries=build/classes/test -Dsonar.junit.reportPaths=build/test-results/test -Dsonar.coverage.jacoco.xmlReportPaths=build/reports/jacoco/test/jacocoTestReport.xml -Dsonar.scm.provider=git -Dsonar.branch.name=${runtimeParams.sourceBranch} -Dsonar.branch.target=${runtimeParams.targetBranch} -Dsonar.links.ci=${env.BUILD_URL}"
                                }
                            } else {
                                // requires SonarQube Scanner 2.8+
                                withSonarQubeEnv('SonarQube Server') {
                                    sh "${scannerHome}/bin/sonar-scanner -X -Dsonar.organization=surgetest -Dsonar.projectKey=surgetest_election-service -Dsonar.projectBaseDir=./${runtimeParams.repositoryName} -Dsonar.sources=src/main/java,src/main/resources -Dsonar.tests=src/test/java -Dsonar.java.libraries=build/libs -Dsonar.java.binaries=build/classes/main -Dsonar.java.test.binaries=build/classes/test -Dsonar.junit.reportPaths=build/test-results/test -Dsonar.coverage.jacoco.xmlReportPaths=build/reports/jacoco/test/jacocoTestReport.xml -Dsonar.scm.provider=git -Dsonar.branch.name=${runtimeParams.targetBranch} -Dsonar.links.ci=${env.BUILD_URL}"
                                }
                            }
                            // wait for quality gate result
                            customRetry (3) {
                                timeout(time: 3, unit: 'MINUTES') { // just in case something goes wrong, pipeline will be killed after a timeout
                                    // works with taskId previously collected by withSonarQubeEnv
                                    def qg = waitForQualityGate()
                                    if (qg.status != 'OK' && isPullRequestCause() && !isPullRequestMergedCause()) {
                                        println "[FAILED] SonarQube quality gate"
                                        error "Pipeline aborted due to quality gate failure: ${qg.status}"
                                    }
                                }
                            }
                        }
                    }
                }
                stage('Smoke Test') {
                    agent any
                    steps {
                        script {
                            println "Setting up smoke tests environment ..."
                            Thread.sleep(30000)
                            println "Running smoke tests ..."
                            Thread.sleep(30000)
                            if (env.BUILD_NUMBER.toInteger() % 2 != 0 && env.BUILD_NUMBER.toInteger() % 3 != 0) {
                                println "[FAILED] Smoke tests"
                                error "Pipeline aborted due to quality gate failure"
                            }
                        }
                    }
                }
            }
        }
        stage('Artifactory') {
            agent any
            // stage won't be skipped as long as uploadArtifacts == true
            when { expression { runtimeParams.uploadArtifacts } }
            steps {
                script {
                    def server = Artifactory.newServer url: runtimeParams.artifactoryServerUrl, credentialsId: 'artiFactoryCredentials'
                    def buildInfo = Artifactory.newBuildInfo()
                    buildInfo.retention maxBuilds: 30, deleteBuildArtifacts: true, async: true
                    println "Uploading ${env.JENKINS_HOME}/jobs/${env.JOB_NAME}/builds/${BUILD_NUMBER}/archive/${runtimeParams.repositoryName}/build/libs/${runtimeParams.artifactVersion}.war ..."
                    println "To ${runtimeParams.repositoryName}/archive/${runtimeParams.majorVersion}/"
                    def uploadSpec = """
                        {
                            "files": [{
                                "pattern": "${env.JENKINS_HOME}/jobs/${env.JOB_NAME}/builds/${BUILD_NUMBER}/archive/${runtimeParams.repositoryName}/build/libs/${runtimeParams.artifactVersion}.war",
                                "target": "${runtimeParams.repositoryName}/archive/${runtimeParams.majorVersion}/"
                            }]
                        }
                    """
                    server.upload spec: uploadSpec, buildInfo: buildInfo
                    server.publishBuildInfo buildInfo
                    if (isPullRequestCause() && isPullRequestMergedCause()) {
                        def promotionConfig = [
                            // Mandatory parameters
                            'buildName'          : buildInfo.name,
                            'buildNumber'        : buildInfo.number,
                            'targetRepo'         : "${runtimeParams.repositoryName}/staging/${runtime.majorVersion}",
                        
                            // Optional parameters
                            'comment'            : 'Promoted build from archive to staging as a release candidate',
                            'sourceRepo'         : "${runtimeParams.repositoryName}/archive/${runtime.majorVersion}",
                            'status'             : 'Released',
                            'includeDependencies': true,
                            'copy'               : true
                        ]
                        Artifactory.addInteractivePromotion server: server, promotionConfig: promotionConfig, displayName: "Do you want to promote ${runtimeParams.artifactVersion} to staging?"
                    }
                }
            }
        }
    }
    post {
        failure {
            bitbucketStatusNotify(buildState: 'FAILED', buildKey: '${BUILD_ID}', buildName: '${JOB_NAME}')
        }
        unstable {
            bitbucketStatusNotify(buildState: 'FAILED', buildKey: '${BUILD_ID}', buildName: '${JOB_NAME}')
        }
        success {
            bitbucketStatusNotify(buildState: 'SUCCESSFUL', buildKey: '${BUILD_ID}', buildName: '${JOB_NAME}')
        }
    }
}