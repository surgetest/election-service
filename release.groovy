@NonCPS
def validateTriggerCause() {
    if (isUserTriggerCause()) {
        def userIdCause = currentBuild.rawBuild.getCause(hudson.model.Cause.UserIdCause)
        if (isValidUser(userIdCause.getUserName())) return true
        else error "[FAILED] ${userIdCause.getUserName()} is not allowed to trigger this pipeline"
    }
    error '[FAILED] Invalid pipeline trigger cause'
}

@NonCPS
def isUserTriggerCause() {
    def userIdCause = currentBuild.rawBuild.getCause(hudson.model.Cause.UserIdCause)
    return userIdCause != null
}

def isValidUser(user) {
    def allowedUsers = ['admin', 'alex', 'matt', 'taiwo']
    return user.toLowerCase() in allowedUsers
}

def prepareVersion(version) {
    def (major, minor, patch) = version.tokenize('.').collect { it as int }

    // TODO: increment version using git change
    patch++
    minor += patch.intdiv(10)
    patch %= 10
    major += minor.intdiv(10)
    minor %= 10

    return [major, minor, patch].join('.')
}

def prettyPrint(object) {
    println groovy.json.JsonOutput.prettyPrint(groovy.json.JsonOutput.toJson(object))
}

def runtimeParams = new LinkedHashMap();

def repositoryNameLabel = 'Repository Name (required)'
def repositoryOwnerLabel = 'Repository Owner (required)'
def targetBranchLabel = 'Target Branch (required)'
def commitLabel = 'Commit (optional)'
def buildAndVerifyLabel = 'Verify Release Branch'
def uploadArtifactsLabel = 'Upload Build Artefact'
def majorVersionLabel = 'Artefact Version'

pipeline {
    agent none
    options {
        buildDiscarder(logRotator(numToKeepStr:'30'))
    }
    parameters {
        string(name: repositoryNameLabel, defaultValue: 'election-service', description: 'Repository Name')
        string(name: repositoryOwnerLabel, defaultValue: 'surgetest', description: 'Repository Owner')
        string(name: targetBranchLabel, defaultValue: 'develop', description: 'Branch e.g. develop, 44c2e9c')
        string(name: commitLabel, defaultValue: '', description: 'Commit e.g. 44c2e9c')
        booleanParam(name: buildAndVerifyLabel, defaultValue: false, description: 'Select to verify release branch')
        booleanParam(name: uploadArtifactsLabel, defaultValue: false, description: 'Select to upload artifacts to Artifactory (Note: automatically builds from repository and target branch)')
        string(name: majorVersionLabel, defaultValue: '1.0.0', description: 'Aterfact Version Label e.g. 1.0.0')
    }
    stages {
        stage('Preparation') {
            agent any
            steps {
                echo 'Aligning the planets and moons!'
                script {
                    lastStage = env.STAGE_NAME
                    // validate trigger cause
                    validateTriggerCause()
                    runtimeParams.buildAndVerify = params[buildAndVerifyLabel]
                    runtimeParams.repositoryName = params[repositoryNameLabel]?.trim()
                    runtimeParams.repositoryOwner = params[repositoryOwnerLabel]?.trim()
                    runtimeParams.targetBranch = params[targetBranchLabel]?.trim()
                    runtimeParams.commit = params[commitLabel]?.trim()
                    runtimeParams.majorVersion = prepareVersion(params[majorVersionLabel]?.trim())
                    runtimeParams.artifactVersion = runtimeParams.majorVersion + '.' + env.BUILD_NUMBER + '-RC'
                    runtimeParams.uploadArtifacts = params[uploadArtifactsLabel]
                    runtimeParams.branchName = "rc-${runtimeParams.majorVersion}"
                    runtimeParams.gitUrl = "git@bitbucket.org:${runtimeParams.repositoryOwner}/${runtimeParams.repositoryName}.git"
                    runtimeParams.artifactoryServerUrl = 'http://35.204.248.170/artifactory'
                    prettyPrint(runtimeParams)
                }
            }
        }
        stage('SCM') {
            agent any
            steps {
                script {
                    lastStage = env.STAGE_NAME
                    git branch: runtimeParams.targetBranch, url: runtimeParams.gitUrl, changelog: true, credentialsId: 'jenkins-ssh'
                    if (runtimeParams.commit) {
                        sh """
                            git checkout ${runtimeParams.commit}
                        """
                    }

                    // N.B. using git checkout -b rather than -B so that an error is propagated when the branch already exists
                    sh """
                        git checkout -b ${runtimeParams.branchName}
                        git tag -a ${runtimeParams.majorVersion} -m \"version ${runtimeParams.majorVersion}\"
                        ./gradlew -b release.gradle prepareGradlePropertiesFile -Pversion=${runtimeParams.majorVersion} --no-daemon
                        git add gradle.properties
                        git commit -m \"Increment version to ${runtimeParams.majorVersion}\"
                    """
                    // git push --set-upstream origin ${runtimeParams.branchName}
                    println "${runtimeParams.branchName} created"
                }
            }
        }
        stage('Build') {
            agent any
            when { expression { runtimeParams.buildAndVerify || runtimeParams.uploadArtifacts } }
            steps {
                script {
                    lastStage = env.STAGE_NAME
                }
                // Run gradle build task
                sh "./gradlew clean jar war -PartifactVersion=${runtimeParams.artifactVersion} --no-daemon"
                // archive artifacts
                archiveArtifacts artifacts: 'build/**/**.jar', fingerprint: true
                archiveArtifacts artifacts: 'build/**/**.war', fingerprint: true
            }
        }
        stage('Unit Test') {
            agent any
            when { expression { runtimeParams.buildAndVerify } }
            steps {
                script {
                    lastStage = env.STAGE_NAME
                }
                // Run the gradle test task
                sh './gradlew test jacocoTestReport --continue --no-daemon && touch build/*/*/TEST-*.xml'
                junit 'build/**/TEST-*.xml'
                jacoco buildOverBuild: true, changeBuildStatus: true, maximumBranchCoverage: '80', maximumClassCoverage: '80', maximumComplexityCoverage: '80', maximumInstructionCoverage: '80', maximumLineCoverage: '80', maximumMethodCoverage: '80', minimumBranchCoverage: '1', minimumClassCoverage: '1', minimumComplexityCoverage: '1', minimumInstructionCoverage: '1', minimumLineCoverage: '1', minimumMethodCoverage: '1', skipCopyOfSrcFiles: true
            }
        }
        stage('Quality Gates') {
            failFast true
            when { expression { runtimeParams.buildAndVerify } }
            parallel {
                stage('SonarQube Analysis') {
                    agent any
                    steps {
                        script {
                            lastStage = env.STAGE_NAME
                            def scannerHome = tool 'SonarQube Scanner';
                            // requires SonarQube Scanner 2.8+
                            withSonarQubeEnv('SonarQube Server') {
                                sh "${scannerHome}/bin/sonar-scanner -X -Dsonar.organization=surgetest -Dsonar.projectKey=surgetest_election-service -Dsonar.projectBaseDir=. -Dsonar.sources=src/main/java,src/main/resources -Dsonar.tests=src/test/java -Dsonar.java.libraries=build/libs -Dsonar.java.binaries=build/classes/main -Dsonar.java.test.binaries=build/classes/test -Dsonar.junit.reportPaths=build/test-results/test -Dsonar.coverage.jacoco.xmlReportPaths=build/reports/jacoco/test/jacocoTestReport.xml -Dsonar.scm.provider=git -Dsonar.branch.name=${runtimeParams.branchName} -Dsonar.links.ci=${env.BUILD_URL}"
                            }
                            // wait for quality gate result
                            customRetry (3) {
                                timeout(time: 3, unit: 'MINUTES') { // just in case something goes wrong, pipeline will be killed after a timeout
                                    // works with taskId previously collected by withSonarQubeEnv
                                    def qg = waitForQualityGate()
                                    if (qg.status != 'OK' && isPullRequestCause() && !isPullRequestMergedCause()) {
                                        println "[FAILED] SonarQube quality gate"
                                        error "Pipeline aborted due to quality gate failure: ${qg.status}"
                                    }
                                }
                            }
                        }
                    }
                }
                stage('Smoke Test') {
                    agent any
                    steps {
                        script {
                            lastStage = env.STAGE_NAME
                            println "Setting up smoke tests environment ..."
                            Thread.sleep(30000)
                            println "Running smoke tests ..."
                            Thread.sleep(30000)
                            if (env.BUILD_NUMBER.toInteger() % 2 != 0 && env.BUILD_NUMBER.toInteger() % 3 != 0) {
                                println "[FAILED] Smoke tests"
                                error "Pipeline aborted due to quality gate failure"
                            }
                        }
                    }
                }
            }
        }
        stage('Artifactory') {
            agent any
            // stage won't be skipped as long as uploadArtifacts == true
            when { expression { runtimeParams.uploadArtifacts } }
            steps {
                script {
                    lastStage = env.STAGE_NAME
                    def server = Artifactory.newServer url: runtimeParams.artifactoryServerUrl, credentialsId: 'artiFactoryCredentials'
                    def buildInfo = Artifactory.newBuildInfo()
                    buildInfo.retention maxBuilds: 30, deleteBuildArtifacts: true, async: true
                    def artifactFilePath = "${env.JENKINS_HOME}/jobs/${runtimeParams.repositoryName}/branches/${env.BRANCH_NAME}/builds/${BUILD_NUMBER}/archive/build/libs/${runtimeParams.repositoryName}-${runtimeParams.artifactVersion}.war"
                    def target = "${runtimeParams.repositoryName}/archive/${runtimeParams.majorVersion}/"
                    println "Uploading ${artifactFilePath} ... \n to ${target}"
                    def uploadSpec = """
                        {
                            "files": [{
                                "pattern": "${artifactFilePath}",
                                "target": "${target}"
                            }]
                        }
                    """
                    server.upload spec: uploadSpec, buildInfo: buildInfo
                    server.publishBuildInfo buildInfo
                    if (isPullRequestCause() && isPullRequestMergedCause()) {
                        def promotionConfig = [
                            // Mandatory parameters
                            'buildName'          : buildInfo.name,
                            'buildNumber'        : buildInfo.number,
                            'targetRepo'         : "${runtimeParams.repositoryName}/staging/${runtimeParams.majorVersion}",
                        
                            // Optional parameters
                            'comment'            : 'Promoted build from archive to staging as a release candidate',
                            'sourceRepo'         : "${runtimeParams.repositoryName}/archive/${runtimeParams.majorVersion}",
                            'status'             : 'Released',
                            'includeDependencies': true,
                            'copy'               : true
                        ]
                        Artifactory.addInteractivePromotion server: server, promotionConfig: promotionConfig, displayName: "Do you want to promote ${runtimeParams.artifactVersion} to staging?"
                    }
                }
            }
        }
    }
    post {
        failure {
            bitbucketStatusNotify(buildState: 'FAILED', buildKey: '${BUILD_ID}', buildName: '${JOB_NAME}')
        }
        unstable {
            bitbucketStatusNotify(buildState: 'FAILED', buildKey: '${BUILD_ID}', buildName: '${JOB_NAME}')
        }
        success {
            bitbucketStatusNotify(buildState: 'SUCCESSFUL', buildKey: '${BUILD_ID}', buildName: '${JOB_NAME}')
        }
    }
}